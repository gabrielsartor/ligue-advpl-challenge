
# LIGUE ADVPL developer test
Teste para seleção de desenvolvedores LIGUE

## Objetivo do teste
Olá, O objetivo deste teste é avaliar o domínio do candidato no desenvolvimento de software. Será avaliado boas práticas de code style, organização do projeto, criação de APIs e tecnologias.

Por se tratar de um teste básico não se esqueça de **MOSTRAR O MAXIMO DE CONHECIMENTO** em boas práticas.
	
## Requisitos
* Deve ser desenvolvido em ADVPL ou TL++.
* Responses devem estar em formato JSON.
* Deve estar apto a rodar em um RPO totvs.
	
## Especificação
	 
Monte uma base de ocorrencias com a seguinte extrutura.
		
```
TABELA:
	SX5010
		
CAMPOS:
	X5_FILIAL - varchar
	X5_TABELA - varchar
	X5_CHAVE - varchar
	X5_DESCRI - varchar
	X5_DESCSPA - varchar
	X5_DESCENG - varchar
	D_E_L_E_T_ - char
	R_E_C_N_O_  - integer - unique
	R_E_C_D_E_L_ - integer
```

Campo de X5_FILIAL deve ser preenchido com 'LG01' e o campo de X5_TABELA com 'ZZ', utilizar o SQL Server como banco de dados.
	
## API endpoints
```
GET /ocurrences
```
Retorna todas as ocorrencias com o X5_TABELA = 'ZZ' e X5_FILIAL = 'LG01'

```
GET /ocurrences/{recno}
```
Retorna os dados de uma ocorrencia a partir de seu R_E_C_N_O_

```
POST /ocurrences
```
Adiciona uma nova ocorrencia

```
PUT /ocurrences/{recno}
```
Atualiza os dados de uma ocorrencia a partir de seu R_E_C_N_O_

```
DELETE /ocurrences/{recno}
```
Delela o registro de uma ocorrencia a partir de seu R_E_C_N_O_ (Se atentar ao padrão TOTVS de exclusão de registros)

## Entrega
Após finalizado o link do projeto, por e-mail, no github com explicação no README.
Não é necessário o envio do arquivo de banco de dados.

**Email para envio do teste e dúvidas:** felipe.souza@ligue.net



		